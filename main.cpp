#include <bits/stdc++.h>

#define FOR(i,a,b) for(int i= a; i<= b; i++)
#define FORD(i,a,b) for(int i= a; i>= b; i--)
#define For(i,a,b) for(int i= a; i< b; i++)
#define Ford(i,a,b) for(int i= a; i> b; i--)
#define FORE(i,v) for (__typeof((v).begin()) i=(v).begin();i!=(v).end();i++)
#define Fill(s,a) memset(s,a,sizeof(s))
#define pb push_back
#define mp make_pair
#define ALL(x) (x).begin(),(x).end()
#define fi first
#define se second

using namespace std;

typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> pii;
typedef unsigned long long ull;

const int N = 500;
const int MaxEdge = 3000;
const double INF = 1e16;

int u[MaxEdge], v[MaxEdge], cap[MaxEdge], m, n;
int cnt[N], lifeTime[N];
int arc[N][N], arc2[N][N], newMatrix[N][N], matrix[N][N];
int numPath, minEdge;
bool finded;
vector<int> path[MaxEdge];
int minEdgePath[MaxEdge];

struct Sensor
{
    /*
    Define a sensor:
        - (x, y): the coordinate of sensor
        - alpha: the sensing offset angle of sensor
        - c: the lifetime of sensor
        - numDirection: the number of vector direction of sensor
        - direction: each element on vector direction indicate the coordinate of a vector direction

    */
    double x, y, r,c;
//    int numDirection;
//    vector<pair<double, double> > direction;
} sen[N];

bool checkOverlap(Sensor S1, Sensor S2)
{
    // TODO
    // Check if sensor S1 with vector direction d1 overlaps sensor S2 with vector direction d2
    double d = (S1.x-S2.x)*(S1.x-S2.x)+(S1.y-S2.y)*(S1.y-S2.y);
    if(d>(S1.r+S2.r)*(S1.r+S2.r)){
        return false;
    }
    return true;
}

class MaxFlowGraph
{
    public:
        int u[MaxEdge], v[MaxEdge], m, n, p[N];
        double cap[MaxEdge], dist[N], f, mf, res[N][N];
        vi adj[N];

    public:
        MaxFlowGraph(int _u[MaxEdge], int _v[MaxEdge], int _cap[MaxEdge], int _m, int _n)
        {
            mf = 0;
            m = _m;
            n = _n;
            FOR(i,1,m)
            {
                u[i] = _u[i];
                v[i] = _v[i];
                cap[i] = _cap[i];
            }

            Fill(res,0);
            FOR(i,1,m)
            {
                adj[u[i]].pb(v[i]);
                res[u[i]][v[i]] += cap[i];
            }
        }

        void printEdge()
        {
            cout << m << endl;
            FOR(i,1,m)
                cout << u[i] << " " << v[i] << " " << cap[i] << endl;
        }

        void augment(int v_, double minEdge)
        {
            if (v_ == 1)
            {
                f = minEdge; return;
            }
            else if (p[v_]!= -1)
            {
                augment(p[v_],min(minEdge, res[p[v_]][v_]));
                res[p[v_]][v_]-= f;
                res[v_][p[v_]]+= f;
            }
        }

        double Edmonds_Karp(int s, int t)
        {
            while (1)
            {
                f = 0;
                FOR(i,1,n) dist[i] = INF;
                dist[s] = 0;
                queue<int> q;
                q.push(s);
                Fill(p,255);
                while (!q.empty())
                {
                    int u_ = q.front();
                    q.pop();
                    if (u_ == t) break;
                    For(i,0,adj[u_].size())
                    {
                        int v_ = adj[u_][i];
                        if (res[u_][v_] > 0 && dist[v_] == INF)
                            dist[v_] = dist[u_] + 1, q.push(v_), p[v_] = u_;
                    }
                }
                augment(t,INF);
                if (f == 0) break;
                mf += f;
            }
            return mf;
        }
};

void DFS(int s)
{
    path[numPath].push_back(s);
    if (s == n)
    {
        finded = true;
        minEdgePath[numPath] = minEdge;
        return;
    }
    FOR(i,1,n)
        if (matrix[s][i] && arc[s][i] > 0)
        {
            minEdge = min(minEdge, arc[s][i]);
            DFS(i);
            arc[s][i] -= minEdge;
            break;
        }
}

void Step_Zero()
{
    freopen("03/03.inp","r",stdin);
    cin>>n;
    FOR(i,2,n+1) cin>>sen[i].x >>sen[i].y >>sen[i].r>>sen[i].c;
    n+=2;
    sen[1].c = INF;
    sen[n].c = INF;
    m =0;
    FOR(i,2,n-2){
        FOR(j,i+1,n-1){
            if(checkOverlap(sen[i],sen[j])){
                m++;
                int l= sen[i].x>sen[j].x?j:i;
                int r = sen[i].x>sen[j].x?i:j;
                u[m] = l;
                v[m] = r;
                matrix[l][r] = 1;
                cap[m] = min(sen[i].c,sen[j].c);
            }
        }
    }
    FOR(i,2,n-1){
        if(sen[i].x<=sen[i].r){
            m++;
            u[m] = 1;
            v[m] = i;
            matrix[1][i] = 1;
            cap[m] = sen[i].c;
        }
        if((sen[i].x+sen[i].r)>=300){
            m++;
            u[m] = i;
            v[m] = n;
            matrix[i][n] = 1;
            cap[m] = sen[i].c;
        }
    }
}

void Step_One()
{
//    freopen("test2.inp","r",stdin);
//    cin >> n >> m;
//    FOR(i,1,n) cin >> lifeTime[i];
//    FOR(i,1,m)
//        cin >> u[i] >> v[i] >> cap[i];

    MaxFlowGraph G = MaxFlowGraph(u, v, cap, m, n);
    double maxFlow = G.Edmonds_Karp(1, n);
    cout << maxFlow << endl;

    Fill(arc,0);

    FOR(i,1,n)
        FOR(j,1,n) arc[i][j] = arc2[i][j] = G.res[j][i];

    while(1)
    {
        numPath++;
        minEdge = INF;
        finded = false;
        DFS(1);
        if (!finded) break;
    }
    numPath--;

    cout << "Num path: " << numPath << endl;

    FOR(ii,1,numPath)
    {
        For(jj,0,path[ii].size())
            cout << path[ii][jj] << " ";
        cout << endl;
    }

    Fill(cnt,0);
    FOR(i,1,numPath)
        For(j,0,path[i].size())
            cnt[path[i][j]]++;
    cnt[1] = cnt[n] = 0;
}

void Step_Two()
{
    int newN = 2 * n;
    Fill(newMatrix,0);
    FOR(i,1,numPath)
    {
        For(j,0,path[i].size()-1)
        {
            int du = path[i][j];
            if (cnt[du] >= 2) du += n;
            int dv = path[i][j+1];
            newMatrix[du][dv] += minEdgePath[i];

            if (cnt[dv] >= 2)
            {
                newMatrix[dv][n + dv] = sen[dv].c;
            }
        }
    }

    int newM = 0;
    FOR(i,1,newN)
        FOR(j,1,newN)
            if (newMatrix[i][j] > 0)
            {
                newM++;
                u[newM] = i;
                v[newM] = j;
                cap[newM] = newMatrix[i][j];
            }
    MaxFlowGraph G1 = MaxFlowGraph(u, v, cap, newM, newN);
    //G1.printEdge();
    cout << G1.Edmonds_Karp(1,n) << endl;
}



int main()
{
    ios_base::sync_with_stdio(false);
    Step_Zero();
    Step_One();
    Step_Two();
    return 0;
}
